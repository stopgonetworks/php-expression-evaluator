<?php
/* =============================================================================
 * PHP Expression Evaluator - Version 1.2 - (c) Stop Go Networks Limited
 * License MPL v2.0 - https://gitlab.com/stopgonetworks/php-expression-evaluator
 *
 * Fork of https://github.com/djfm/Evalua1tor by François-Marie de Jouvencel
 * ========================================================================== */

declare(strict_types=1);

class stopgonetworks_evaluator
{

  public $error = '';

  /* operators ordered by precedence and with their arity */
  private $operators = [
    '!' => 1,
    '^' => 2,
    '/' => 2,
    '*' => 2,
    '+' => 2,
    '-' => 2,
    '<' => 2,
    '>' => 2,
    '<=' => 2,
    '>=' => 2,
    '!=' => 2,
    '==' => 2,
    '&&' => 2,
    '||' => 2,
  ];

  public function evaluate(string $expression, array $arguments = [])
  {
    try {
      if ('' === $expression) {
        throw new exception('Empty expression');
      }
      /* add negative numbers to original '/\b\d+(?:\.\d+)?\b/' */
      $number = '/-?\b\d+(?:\.\d+)?\b|"(.*?)"|(true|false)/'; //
      /* add <namespace.variable> schema to orginal '/\$\w+/' */
      $variable = '/(?<![\w\d\.\"\[\]$])(?!true|false)[A-Za-z][\w$]*(\.[\w$]+)?(\[\d+])?(?![\w\d\.\[\]$])/';
      /* add exclude negative numbers to original '/[\!&\|+\-<>=\\/\*\^]+/' */
      $operator = '/[\!&\|+<>=\\/\*\^]+|\-(?!\d)+/';

      /* replace numbers */
      $matches = [];
      preg_match_all($number, $expression, $matches);
      $numbers = [];
      foreach ($matches[0] as $value) {
        switch ($value) {
          case 'true':
            $cast = true;
            break;
          case 'false':
            $cast = false;
            break;
          default:
            $cast = str_replace('"', '', $value);
            break;
        }
        $numbers[] = $cast;
      }

      /* replace variables */
      preg_match_all($variable, $expression, $matches);
      $variables = $matches[0];

      /* replace operators */
      preg_match_all($operator, $expression, $matches);
      $operators = $matches[0];

      $expression = preg_replace($variable, "v", $expression);
      $expression = preg_replace($number, "n", $expression);
      $expression = preg_replace($operator, "o", $expression);
      $expression = str_replace('$', '', $expression);

      $nodes = [];
      $group = &$nodes;
      $stack = [];
      for ($i = 0; $i < strlen($expression); $i += 1) {
        if ($expression[$i] == 'v') {
          $group[] = ['type' => 'variable', 'value' => array_shift($variables)];
        } elseif ($expression[$i] == 'n') {
          $group[] = ['type' => 'number', 'value' => array_shift($numbers)];
        } elseif ($expression[$i] == 'o') {
          $group[] = ['type' => 'operator', 'value' => array_shift($operators)];
        } elseif ($expression[$i] == '(') {
          if (isset($elements)) {
            unset($elements);
          }
          $elements = [];
          $subgroup = ['type' => 'group', 'nodes' => &$elements];
          $group[] = $subgroup;
          $stack[] = &$group;
          unset($group);
          $group = &$elements;
        } elseif ($expression[$i] == ')') {
          $top = &$stack[count($stack) - 1];
          array_pop($stack);
          $group = &$top;
        } else {
          // to do: WHAT IS IT?
        }
      }

      $nodes = ['type' => 'group', 'nodes' => $nodes];
      $this->canonicalize($nodes);
      $this->apply_precedence($nodes);
      $this->canonicalize($nodes);

      $this->ast = $nodes;
      $result = $this->reduce($this->ast, $arguments);
    } catch (exception $ex) {
      $result = null;
      $this->error = $ex->getmessage();
    }
    return $result;
  }

  private function compute($operator, $arguments)
  {
    if ($operator == '!') {
      return !$arguments[0];
    } else if ($operator == '/') {
      /* prevent division by 0 */
      if (0 == $arguments[1]) {
        throw new exception('Division by zero');
      }
      $result = $arguments[0] / $arguments[1];
      return $result + 0;
    } else if ($operator == '^') {
      return ($arguments[0] ** $arguments[1]) + 0;
    } else if ($operator == '*') {
      return ($arguments[0] * $arguments[1]) + 0;
    } else if ($operator == '-') {
      return ($arguments[0] - $arguments[1]) + 0;
    } else if ($operator == '+') {
      return ($arguments[0] + $arguments[1]) + 0;
    } else if ($operator == '&&') {
      return (bool) ($arguments[0] && $arguments[1]);
    } else if ($operator == '||') {
      return (bool) ($arguments[0] || $arguments[1]);
    } else if ($operator == '<') {
      return (bool) ($arguments[0] < $arguments[1]);
    } else if ($operator == '>') {
      return (bool) ($arguments[0] > $arguments[1]);
    } else if ($operator == '<=') {
      return (bool) ($arguments[0] <= $arguments[1]);
    } else if ($operator == '>=') {
      return (bool) ($arguments[0] >= $arguments[1]);
    } else if ($operator == '!=') {
      return (bool) ($arguments[0] != $arguments[1]);
    } else if ($operator == '==') {
      return (bool) ($arguments[0] == $arguments[1]);
    } else {
      throw new exception("Unknown operator $operator!");
    }
  }

  private function reduce($node, $arguments)
  {
    if ($node['type'] == 'application') {
      $ops = [];
      foreach ($node['operands'] as $operand) {
        $ops[] = $this->reduce($operand, $arguments);
      }
      return $this->compute($node['operator'], $ops);
    } else if ($node['type'] == 'number') {
      return $node['value'];
    } else if ($node['type'] == 'variable') {
      if (isset($arguments[$node['value']])) {
        return $arguments[$node['value']];
      } else {
        throw new exception("Variable " . $node['value'] . " was not assigned!");
      }
    } else {
      throw new exception("Don't know how to reduce node with type " . $node['type']);
    }
  }

  private function to_string($node): string
  {
    if ($node['type'] == 'group') {
      return '[ ' . implode(' ', array_map([$this, 'to_string'], $node['nodes'])) . ' ]';
    } else if ($node['type'] == 'application') {
      if ($this->operators[$node['operator']] == 1) {
        return '( ' . $node['operator'] . $this->to_string($node['operands'][0]) . ' )';
      } else {
        return '( ' . $this->to_string($node['operands'][0]) . ' ' . $node['operator'] . ' ' . $this->to_string($node['operands'][1]) . ' )';
      }
    } else {
      return $node['value'];
    }
  }

  //remove superfluous parentheses
  private function canonicalize(&$node)
  {
    if ($node['type'] == 'group') {
      foreach ($node['nodes'] as &$child) {
        $this->canonicalize($child);
      }
      if (count($node['nodes']) == 1) {
        $node = $node['nodes'][0];
      }
    } else if ($node['type'] == 'application') {
      foreach ($node['operands'] as &$child) {
        $this->canonicalize($child);
      }
    }
  }

  private function apply_precedence(&$node)
  {
    if ($node['type'] == 'group') {
      foreach ($node['nodes'] as &$child) {
        $this->apply_precedence($child);
      }
      foreach ($this->operators as $operator => $arity) {
        do {
          $index = -1;
          for ($i = 0; $i < count($node['nodes']); $i += 1) {
            if (($node['nodes'][$i]['type'] == 'operator') and ( $node['nodes'][$i]['value'] == $operator)) {
              $index = $i;
              break;
            }
          }
          if ($index >= 0) {
            if ($arity == 1) {
              $new_nodes = array_slice($node['nodes'], 0, $index);
              $operands = [$node['nodes'][$index + 1]];
            } else {
              $new_nodes = array_slice($node['nodes'], 0, $index - 1);
              if (!isset($node['nodes'][$index - 1])) {
                throw new exception('Invalid index ' . $index);
              }
              $operands = [$node['nodes'][$index - 1], $node['nodes'][$index + 1]];
            }
            $new_nodes[] = ['type' => 'application', 'operator' => $operator, 'operands' => $operands];
            $new_nodes = array_merge($new_nodes, array_slice($node['nodes'], $index + 2));
            $node['nodes'] = $new_nodes;
          }
        } while ($index >= 0);
      }
    }
  }

}