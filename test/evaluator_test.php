<?php
/* =============================================================================
 * PHP Expression Evaluator - Version 1.2 - (c) Stop Go Networks Limited
 * License MPL v2.0 - https://gitlab.com/stopgonetworks/php-expression-evaluator
 *
 * Fork of https://github.com/djfm/Evalua1tor by François-Marie de Jouvencel
 * ========================================================================== */


use bateo_test as test;
use stopgonetworks_evaluator as evaluator;

class bateo_testcase implements bateo_testcase_interface
{

  public $evaluator;

  public function setup()
  {
    include '../src/evaluator.php';
    include __DIR__ . '/dump.php';
    $this->evaluator = new evaluator();
  }

  public function teardown()
  {

  }

  public function t_empty_expression(test $t)
  {
    $t->wie = null;
    $t->wig = $this->evaluator->evaluate('');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_expression_without_spaces(test $t)
  {
    $t->wie = 2;
    $t->wig = $this->evaluator->evaluate('4/2');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_expression_with_spaces(test $t)
  {
    $t->wie = 2;
    $t->wig = $this->evaluator->evaluate('4 / 2');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_division_with_integer_result(test $t)
  {
    $t->wie = 2;
    $t->wig = $this->evaluator->evaluate('4/2');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_division_with_float_result(test $t)
  {
    $t->wie = 0.5;
    $t->wig = $this->evaluator->evaluate('2/4');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_multiplication_with_integer_result(test $t)
  {
    $t->wie = 8;
    $t->wig = $this->evaluator->evaluate('4 * 2');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_multiplication_with_float_result(test $t)
  {
    $t->wie = 5.0;
    $t->wig = $this->evaluator->evaluate('2.5 * 2');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_addition_with_integer_result(test $t)
  {
    $t->wie = 6;
    $t->wig = $this->evaluator->evaluate('4 + 2');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_addition_with_float_result(test $t)
  {
    $t->wie = 4.5;
    $t->wig = $this->evaluator->evaluate('2.5 + 2');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_subtraction_with_integer_result(test $t)
  {
    $t->wie = 2;
    $t->wig = $this->evaluator->evaluate('4 - 2');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_subtraction_with_float_result(test $t)
  {
    $t->wie = 0.5;
    $t->wig = $this->evaluator->evaluate('2.5 - 2');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_index_with_integer_result(test $t)
  {
    $t->wie = 16;
    $t->wig = $this->evaluator->evaluate('4 ^ 2');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_index_with_float_result(test $t)
  {
    $t->wie = 6.25;
    $t->wig = $this->evaluator->evaluate('2.5 ^ 2');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_index_complex(test $t)
  {
    $t->wie = 18;
    $t->wig = $this->evaluator->evaluate('2 * 3 ^ 2');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_brackets_with_integer_result(test $t)
  {
    $t->wie = 10;
    $t->wig = $this->evaluator->evaluate('(4 * 2) + (1 + 3) / 2');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_brackets_with_float_result(test $t)
  {
    $t->wie = 7.0;
    $t->wig = $this->evaluator->evaluate('(2.5 * 2) + (1 + 3) / 2');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_logic_or_true(test $t)
  {
    $t->wie = true;
    $t->wig = $this->evaluator->evaluate('0 || 3');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_logic_or_false(test $t)
  {
    $t->wie = false;
    $t->wig = $this->evaluator->evaluate('0 || 0');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_logic_and_true(test $t)
  {
    $t->wie = true;
    $t->wig = $this->evaluator->evaluate('1 && 1');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_logic_and_false(test $t)
  {
    $t->wie = false;
    $t->wig = $this->evaluator->evaluate('1 && 0');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_greater_than_true(test $t)
  {
    $t->wie = true;
    $t->wig = $this->evaluator->evaluate('5 > 1');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_greater_than_false(test $t)
  {
    $t->wie = false;
    $t->wig = $this->evaluator->evaluate('5 > 5');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_greater_or_equal_than_true(test $t)
  {
    $t->wie = true;
    $t->wig = $this->evaluator->evaluate('5 >= 5');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_greater_or_equal_than_false(test $t)
  {
    $t->wie = false;
    $t->wig = $this->evaluator->evaluate('5 >= 6');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_less_than_true(test $t)
  {
    $t->wie = true;
    $t->wig = $this->evaluator->evaluate('1 < 5');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_less_than_false(test $t)
  {
    $t->wie = false;
    $t->wig = $this->evaluator->evaluate('5 < 1');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_less_or_equal_than_true(test $t)
  {
    $t->wie = true;
    $t->wig = $this->evaluator->evaluate('5 <= 5');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_less_or_equal_than_false(test $t)
  {
    $t->wie = false;
    $t->wig = $this->evaluator->evaluate('6 <= 5');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_not_equal_to_true(test $t)
  {
    $t->wie = true;
    $t->wig = $this->evaluator->evaluate('6 != 5');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_not_equal_to_false(test $t)
  {
    $t->wie = false;
    $t->wig = $this->evaluator->evaluate('6 != 6');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_equal_to_true(test $t)
  {
    $t->wie = true;
    $t->wig = $this->evaluator->evaluate('6 == 6');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_equal_to_false(test $t)
  {
    $t->wie = false;
    $t->wig = $this->evaluator->evaluate('6 == 5');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_negative_number(test $t)
  {
    $t->wie = -12.0;
    $t->wig = $this->evaluator->evaluate('-6 * 2.0');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_negative_number_sum(test $t)
  {
    $t->wie = -3;
    $t->wig = $this->evaluator->evaluate('-6 - -3');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_arguments_simple(test $t)
  {
    $t->wie = 6;
    $t->wig = $this->evaluator->evaluate('amount + 3', ['amount' => 3]);
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_arguments_complex(test $t)
  {
    $t->wie = 179751035747363528;
    $t->wig = $this->evaluator->evaluate(
      'mass * speed_of_light ^ 2',
      [
        'speed_of_light' => 299792458,
        'mass' => 2
      ]
    );
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_only_boolean_argument(test $t)
  {
    $t->wie = true;
    $t->wig = $this->evaluator->evaluate('is_open', ['is_open' => true]);
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_only_integer_argument(test $t)
  {
    $t->wie = 1; // must return an integer and not true!
    $t->wig = $this->evaluator->evaluate('my_integer', ['my_integer' => 1]);
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_only_float_argument(test $t)
  {
    $t->wie = 1.2; // must return a float and not true!
    $t->wig = $this->evaluator->evaluate('my_float', ['my_float' => 1.2]);
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_expression_with_arguments_namespaced(test $t)
  {
    $t->wie = 2.2;
    $t->wig = $this->evaluator->evaluate('ns1.my_float + ns2.my_float', ['ns1.my_float' => 1.2, 'ns2.my_float' => 1]);
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_division_by_zero(test $t)
  {
    $t->wie = null;
    $t->wig = $this->evaluator->evaluate('10 / 0');
    $t->pass_if($t->wie === $t->wig && '' !== $this->evaluator->error, bateo_wix($t->wie, $t->wig));
  }

  public function t_invalid_argument(test $t)
  {
    $t->wie = null;
    $t->wig = $this->evaluator->evaluate('my_int', ['my_float' => 1.2]);
    $t->pass_if($t->wie === $t->wig && '' !== $this->evaluator->error, bateo_wix($t->wie, $t->wig));
  }

  public function t_arguments_complex_brackets_true(test $t)
  {
    $t->wie = true;
    $arr = ['l.am' => 750, 'l.ce' => 100, 'l.he' => 150];
    $t->wig = $this->evaluator->evaluate('l.am == 750 && ( l.ce == 100 || l.he == 100)', $arr);
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_arguments_complex_brackets_false(test $t)
  {
    $t->wie = false;
    $arr = ['l.am' => 750, 'l.ce' => 230, 'l.he' => 150];
    $t->wig = $this->evaluator->evaluate('l.am == 750 && ( l.ce == 100 || l.he == 100)', $arr);
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_arguments_complex_brackets2_true(test $t)
  {
    $t->wie = true;
    $arr = ['l.am' => 750, 'l.ce' => 100, 'l.he' => 150];
    $t->wig = $this->evaluator->evaluate('l.am == 750 && (((l.ce == 100) || (l.he == 100)))', $arr);
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_arguments_complex_brackets2_false(test $t)
  {
    $t->wie = false;
    $arr = ['l.am' => 750, 'l.ce' => 230, 'l.he' => 150];
    $t->wig = $this->evaluator->evaluate('l.am == 750 && (((l.ce == 100) || (l.he == 100)))', $arr);
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_arguments_complex_brackets_without_spaces_true(test $t)
  {
    $t->wie = true;
    $arr = ['l.am' => 750, 'l.ce' => 100, 'l.he' => 150];
    $t->wig = $this->evaluator->evaluate('l.am==750&&(((l.ce == 100)||(l.he == 100)))', $arr);
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_arguments_complex_brackets_with_spaces_false(test $t)
  {
    $t->wie = false;
    $arr = ['l.am' => 750, 'l.ce' => 230, 'l.he' => 150];
    $t->wig = $this->evaluator->evaluate('l.am==750&&(((l.ce == 100)||(l.he == 100)))', $arr);
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_arguments_bidmas_bracket(test $t)
  {
    $t->wie = -21;
    $arr = [];
    $t->wig = $this->evaluator->evaluate('1 - 4 + (6 / 3) * (3 ^ 2)', $arr);
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_arguments_bidmas_without_bracket(test $t)
  {
    $t->wie = -21;
    $arr = [];
    $t->wig = $this->evaluator->evaluate('1 - 4 + 6 / 3 * 3 ^ 2', $arr);
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_arguments_bidmas_without_index(test $t)
  {
    $t->wie = -9;
    $arr = [];
    $t->wig = $this->evaluator->evaluate('1 - 4 + 6 / 3 * 3', $arr);
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_equals_with_string_true(test $t)
  {
    $t->wie = true;
    $t->wig = $this->evaluator->evaluate(
      'l.name == "david"',
      ['l.name' => "david", 'l.surname' => "miler"]
    );
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_equals_with_string_false(test $t)
  {
    $t->wie = false;
    $t->wig = $this->evaluator->evaluate(
      'l.name == "miler"',
      ['l.name' => "david", 'l.surname' => "miler"]
    );
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_not_equals_with_string_true(test $t)
  {
    $t->wie = true;
    $t->wig = $this->evaluator->evaluate(
      'l.name != "miler"',
      ['l.name' => "david", 'l.surname' => "miler"]
    );
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_not_equals_with_string_false(test $t)
  {
    $t->wie = false;
    $t->wig = $this->evaluator->evaluate(
      'l.name != "david"',
      ['l.name' => "david", 'l.surname' => "miler"]
    );
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_strings_and_numbers_inverted_position(test $t)
  {
    $t->wie = false;
    $t->wig = $this->evaluator->evaluate(
      'l.name != "david" && age > 30',
      ['l.name' => "david", 'age' => 28]
    );
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_boolean_true(test $t)
  {
    $t->wie = true;
    $t->wig = $this->evaluator->evaluate('true');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_boolean_false(test $t)
  {
    $t->wie = false;
    $t->wig = $this->evaluator->evaluate('false');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_arguments_boolean_true(test $t)
  {
    $t->wie = true;
    $t->wig = $this->evaluator->evaluate('is_active == true', ['is_active' => true]);
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_arguments_boolean_false(test $t)
  {
    $t->wie = false;
    $t->wig = $this->evaluator->evaluate('is_active == true', ['is_active' => false]);
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_not_operator_true(test $t)
  {
    $t->wie = true;
    $t->wig = $this->evaluator->evaluate('!false');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_not_operator_false(test $t)
  {
    $t->wie = false;
    $t->wig = $this->evaluator->evaluate('!true');
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_not_operator_arguments(test $t)
  {
    $t->wie = false;
    $t->wig = $this->evaluator->evaluate('!is_active', ['is_active' => true]);
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_arguments_with_numbers_booleans_strings(test $t)
  {
    $t->wie = true;
    $t->wig = $this->evaluator->evaluate(
      'is_active == true && name == "andrea" && age > 10 && age <= 30',
      [
        'is_active' => true,
        'name' => 'andrea',
        'age' => 28,
      ]
    );
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

  public function t_arguments_with_numbers_booleans_strings_and_not_operator(test $t)
  {
    $t->wie = true;
    $t->wig = $this->evaluator->evaluate(
      '!is_dead && name == "james" && age > 90 && age <= 120',
      [
        'is_dead' => false,
        'name' => 'james',
        'age' => 97,
      ]
    );
    $t->pass_if($t->wie === $t->wig, bateo_wix($t->wie, $t->wig));
  }

}
